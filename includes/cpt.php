<?php
use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

function nni_cpt() {

	/* Property */
	$labels = array(
		'name'                => _x('Artistes', 'Post Type General Name', 'nni-plugin'),
		'singular_name'       => _x('Artiste', 'Post Type Singular Name', 'nni-plugin'),
		'menu_name'           => __('Artistes', 'nni-plugin'),
		'name_admin_bar'      => __('Artistes', 'nni-plugin'),
		'parent_item_colon'   => __('Parent :', 'nni-plugin'),
		'all_items'           => __('Tous les artistes', 'nni-plugin'),
		'add_new_item'        => __('Ajouter nouvel artiste', 'nni-plugin'),
		'add_new'             => __('Ajouter nouveau', 'nni-plugin'),
		'new_item'            => __('Nouvel artiste', 'nni-plugin' ),
		'edit_item'           => __('Modifier artiste Item', 'nni-plugin'),
		'update_item'         => __('Modifier artiste', 'nni-plugin'),
		'view_item'           => __('Voir artiste Item', 'nni-plugin'),
		'search_items'        => __('Rechercher artiste', 'nni-plugin'),
		'not_found'           => __('Non trouvé', 'nni-plugin'),
		'not_found_in_trash'  => __('Non trouvé dans la corbeille', 'nni-plugin'),
	);
	$args = array(
		'label'               => __('nni_artist', 'nni-plugin'),
		'description'         => __('Artistes', 'nni-plugin'),
		'labels'              => $labels,
		'supports'            => array('title', 'thumbnail'),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 25,
		'menu_icon'           => 'dashicons-instagram',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
        'capability_type' => 'post',
        // 'capabilities' => array(
        //   'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
        // ),
        // 'map_meta_cap' => true, // Set to `false`, if users are not allowed to edit/delete existing posts
	);
	register_post_type('nni_artist', $args);
}

add_action('init', 'nni_cpt', 10);

add_action( 'template_redirect', 'nni_redirect_artist' );
function nni_redirect_artist()
{
	if ( ! is_singular( 'nni_artist' ) )
		return;

    wp_redirect(home_url());
	exit;
}

// Add the custom columns to the book post type:
add_filter( 'manage_nni_artist_posts_columns', 'nni_set_custom_edit_nni_artist_columns' );
function nni_set_custom_edit_nni_artist_columns($columns) {
    $date = $columns['date'];
    unset( $columns['author'] );
    unset( $columns['date'] );
    // $columns['hashtags'] = __( 'Hashtags', 'nni-plugin' );
    $columns['links'] = __( 'Liens', 'nni-plugin' );
    $columns['featured_image'] = __( 'Photo', 'nni-plugin' );
    $columns['date'] = $date;

    return $columns;
}

// Add the data to the custom columns for the book post type:
add_action( 'manage_nni_artist_posts_custom_column' , 'nni_custom_nni_artist_column', 10, 2 );
function nni_custom_nni_artist_column( $column, $post_id ) {
    switch ( $column ) {
        // case 'hashtags' :
        //     $hashtags = carbon_get_post_meta($post_id, 'nni_artist_instagram_hashtags');
        //     echo $hashtags;
        //     break;
        case 'links' :
            $links = array();
            $link = carbon_get_post_meta($post_id, 'nni_artist_instagram');
            if(!empty(trim($link))) {
                $links[] = '<a href="https://www.instagram.com/'.trim($link).'" target="_blank">Instagram @'.trim($link).'</a>';
            }
            $link = carbon_get_post_meta($post_id, 'nni_artist_facebook');
            if(!empty(trim($link))) {
                $links[] = '<a href="'.trim($link).'" target="_blank">Facebook</a>';
            }
            $link = carbon_get_post_meta($post_id, 'nni_artist_linkedin');
            if(!empty(trim($link))) {
                $links[] = '<a href="'.trim($link).'" target="_blank">LinkedIn</a>';
            }
            $link = carbon_get_post_meta($post_id, 'nni_artist_www');
            if(!empty(trim($link))) {
                $links[] = '<a href="'.trim($link).'" target="_blank">Site Internet</a>';
            }
            echo '<p>'.implode('</p><p>', $links).'</p>';
            break;
        case 'featured_image':
            echo get_the_post_thumbnail($post_id, 'thumbnail' ); 
            break;
    }
}