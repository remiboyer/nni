<?php
// photosniconova@gmail.com
namespace Mosaika\Plugin\Options;
use WP_Dependency_Installer;
use EspressoDev\InstagramBasicDisplay\InstagramBasicDisplay;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;
use stdClass;

global $nni_artists_fields;
$nni_artists_fields = array();

/**
 * Créée une page d'options pour notre plugin.
 * Les onglets sont initialement vides mais sont définis et remplis de champs via des filtres définis plus bas.
 *
 * @link https://carbonfields.net/docs/containers-theme-options/
 *
 * @return void
 */
function options_initialize_admin_page() {


    add_image_size( 'nni_lazy', 50, 50 );
    add_image_size( 'nni_large', 800, 800 );

    global $nni_artists_fields;

    $fields = [];

	$fields[] = Field::make( 'text', 'nni_artist_facebook', __( "URL Facebook", 'nni-plugin' ));
    $nni_artists_fields[] = 'nni_artist_facebook';
	$fields[] = Field::make( 'text', 'nni_artist_linkedin', __( "URL Linkedin", 'nni-plugin' ));
    $nni_artists_fields[] = 'nni_artist_linkedin';
	$fields[] = Field::make( 'text', 'nni_artist_www', __( "URL Site Internet", 'nni-plugin' ));
    $nni_artists_fields[] = 'nni_artist_www';
	$fields[] = Field::make( 'text', 'nni_artist_video', __( "Lien vers bande-démo", 'nni-plugin' ));
    $nni_artists_fields[] = 'nni_artist_video';
	$fields[] = Field::make( 'text', 'nni_artist_instagram', __( "URL Instagram", 'nni-plugin' ))->set_help_text( 'Champ automatiquement renseigné lors de la synchronisation avec les publications Instagram' );
    $nni_artists_fields[] = 'nni_artist_instagram';


    Container::make( 'post_meta', __( 'Extra' ) )
	->where( 'post_type', '=', 'nni_artist' )
	->add_fields( $fields );


	$fields = [];

	$fields[] = Field::make( 'text', 'nni_artist_instagram_id', __( "ID publication Instagram", 'nni-plugin' ) );
    $nni_artists_fields[] = 'nni_artist_instagram_id';

	$fields[] = Field::make( 'text', 'nni_artist_instagram_timestamp', __( "Date publication Instagram", 'nni-plugin' ) );
    $nni_artists_fields[] = 'nni_artist_instagram_timestamp';

	$fields[] = Field::make( 'textarea', 'nni_artist_instagram_json', __( "JSON Instagram", 'nni-plugin' ) );
    $nni_artists_fields[] = 'nni_artist_instagram_json';

	$fields[] = Field::make( 'textarea', 'nni_artist_instagram_permalink', __( "Permalink Instagram", 'nni-plugin' ) );
    $nni_artists_fields[] = 'nni_artist_instagram_permalink';

	$fields[] = Field::make( 'textarea', 'nni_artist_instagram_image_url', __( "Image URL Instagram", 'nni-plugin' ) );
    $nni_artists_fields[] = 'nni_artist_instagram_image_url';

	$fields[] = Field::make( 'textarea', 'nni_artist_instagram_hashtags', __( "Hashtags", 'nni-plugin' ) );
    $nni_artists_fields[] = 'nni_artist_instagram_hashtags';

	// $fields[] = Field::make( 'complex', 'nni_artist_instagram_other_images', __( "Autres images", 'nni-plugin' ) )
    //     ->add_fields( array(
    //         Field::make( 'image', 'image', __( 'Image', 'nni-plugin' ) ),
    //         Field::make( 'text', 'instagram_url', __( 'Instagram URL', 'nni-plugin' ) ),
    //     ) );
    // $nni_artists_fields[] = 'nni_artist_instagram_other_images';

    Container::make( 'post_meta', __( 'Instagram' ) )
    ->where( 'post_type', '=', 'nni_artist' )
    ->add_fields( $fields );


	$tabs = apply_filters( 'nni_plugin_options_tabs', [] );

	if ( empty( $tabs ) ) {
		return;
	}

	// On créée la page d'options.
	$theme_options = Container::make( 'theme_options', __( 'Options du plugin', 'nni-plugin' ) );

	// On définit son slug utilisé dans l'URL de la page.
	$theme_options->set_page_file( 'plugin-nni' );

	// On définit son nom dans le menu d'admin.
	$theme_options->set_page_menu_title( __( 'NicoNova Insta', 'nni-plugin' ) );

	// On définit sa position dans le menu d'admin.
	$theme_options->set_page_menu_position( 31 );

	// On change son icône dans le menu d'admin.
	$theme_options->set_icon( 'dashicons-instagram' );

	// Et enfin, pour chaque onglet, on charge les champs de l'onglet concerné.
	foreach ( $tabs as $tab_slug => $tab_title ) {
		$theme_options->add_tab(
			esc_html( $tab_title ),
			apply_filters( "nni_plugin_options_fields_tab_{$tab_slug}", [] )
		);
	}
    carbon_set_post_meta( 10, 'crb_text', 'Hello World!' );

    if(isset($_REQUEST['refresh']) && $_REQUEST['refresh'] = 1) {
        nni_get_artists(false);
    }
    if(isset($_REQUEST['sync']) && $_REQUEST['sync'] = 1) {

        $page = 1;
        if(isset($_REQUEST['page']) && intval($_REQUEST['page']) > 0) {
            $page = intval($_REQUEST['page']);
        }
        nni_start_sync($page);
    }

    if(isset($_GET['code'])) {
        $instagram = new InstagramBasicDisplay([
            'appId' => \carbon_get_theme_option( 'nni_instagram_app_id' ),
            'appSecret' => \carbon_get_theme_option( 'nni_instagram_app_secret' ),
            'redirectUri' => admin_url()
        ]);
        // Get the OAuth callback code
        $code = $_GET['code'];

        // Get the short lived access token (valid for 1 hour)
        $token = $instagram->getOAuthToken($code, true);
        if(!empty($token)) {
            \carbon_set_theme_option('nni_instagram_short_token', $token);
            // echo 'Your short token is: ' . $token;

            // Exchange this token for a long lived token (valid for 60 days)
            $token = $instagram->getLongLivedToken($token, true);
            if(!empty($token)) {
                \carbon_set_theme_option('nni_instagram_long_token', $token);
                add_action( 'admin_notices', 'nni_get_code_success' );
                wp_redirect(admin_url('admin.php?page=plugin-nni'));        
                // echo 'Your long token is: ' . $token;
            }
        }
        add_action( 'admin_notices', 'nni_get_code_error' );
        wp_redirect(admin_url('admin.php?page=plugin-nni'));
    }
}
add_action( 'carbon_fields_register_fields', __NAMESPACE__ . '\\options_initialize_admin_page' );

function nni_start_sync($page = 1, $is_cron = false) {
    
    $token = \carbon_get_theme_option('nni_instagram_long_token');
        
    if(!empty($token)) {
        $all_data = $args = get_posts( array(
            'numberposts' => -1,
            'post_type'   => 'nni_artist'
        ) );
        $existing_data = [];
        foreach($all_data as $d) {
            $i_id = carbon_get_post_meta( $d->ID, 'nni_artist_instagram_id' );
            if($i_id > 0) {
                $existing_data[$i_id] = $d->ID;
            }
        }


        // Page

        // Nb pages
        $instagram_medias = array();
        
        $nb_pages = 0;
        $nb_total_artistes = 0;
        $instagram_api_data = json_decode(get_transient('nni_instagram_api_json'));
        if(!is_array($instagram_api_data) || (is_array($instagram_api_data) && sizeof($instagram_api_data) == 0)) {
            
            $instagram = new InstagramBasicDisplay([
                'appId' => \carbon_get_theme_option( 'nni_instagram_app_id' ),
                'appSecret' => \carbon_get_theme_option( 'nni_instagram_app_secret' ),
                'redirectUri' => admin_url()
            ]);
            $instagram->setAccessToken($token);
            $instagram->setMediaFields('caption, id, media_type, media_url, timestamp, permalink, children{media_type, media_url, timestamp, thumbnail_url}');


            $media = $instagram->getUserMedia();
            if(isset($media->error->message)) {
                die(json_encode(array('error' => $media->error->message)));
            }
            if(!is_null($media->data) && sizeof($media->data) > 0) {
                $moreMedia = $media;
                $instagram_medias[] = $moreMedia;
                $nb_pages ++;
                $nb_total_artistes += sizeof($moreMedia->data);
                while(2 > 1) {
                    $moreMedia = $instagram->pagination($moreMedia);
                    if(!is_null($moreMedia->data) && sizeof($moreMedia->data) > 0) {
                        $instagram_medias[] = $moreMedia;   
                        $nb_pages ++;    
                        $nb_total_artistes += sizeof($moreMedia->data);                 
                    }else {
                        break;
                    }
                }
            }
            $new_data = array();
            foreach($instagram_medias as $k=>$instagram_media) {
                if(isset($instagram_media->data) && is_array($instagram_media->data) && sizeof($instagram_media->data) > 0) {
                    $new_data = array_merge($new_data, array_chunk($instagram_media->data, 3));  
                }                  
            }
            set_transient('nni_instagram_api_json', json_encode($new_data), 3600);
            $instagram_api_data = $new_data;
            
        }
        $nb_pages = sizeof($instagram_api_data);
        set_transient('nni_instagram_nb_pages_api_json', $nb_pages, 3600);
        
        delete_transient('nni_instagram_current_page_api_json');
        set_transient('nni_instagram_current_page_api_json', $page, 3600);
        foreach($instagram_api_data as $k=>$data) {
            if(($k + 1) == $page) {
                $r = nni_update_artists($data, $existing_data);
                $finish = ($page >= sizeof($instagram_api_data)) ? 1 : 0;
                $page ++;
                if($finish == 1) {
                    nni_get_artists(false);
                }
                if($is_cron) {
                    if($finish) {
                        delete_transient('nni_instagram_current_page_api_json');
                    }else {
                        delete_transient('nni_instagram_current_page_api_json');
                        set_transient('nni_instagram_current_page_api_json', $page, 3600);
                        wp_schedule_single_event( time() + 5, 'nni_cron_instagram_api_sync_next_command');
                    }
                }
                die(json_encode(array('finish' => $finish, 'page' => $page, 'nb_pages' => $nb_pages.' : '.$r)));
            }
        }
        nni_get_artists(false);
        if($is_cron) {
            delete_transient('nni_instagram_current_page_api_json');
        }
        die(json_encode(array('finish' => 1, 'page' => $page, 'nb_pages' => $nb_pages)));
        // if(!is_null($media->data)) {
        //     $moreMedia = $media;
        //     $k = 1;
        //     while(2 > 1) {
        //         if(!is_null($moreMedia->data) && sizeof($moreMedia->data) > 0) {
        //             if($k == $page) {
        //                 $r = nni_update_artists($moreMedia, $existing_data, $instagram, $page);
        //                 $finish = ($r) ? 0 : 1;
        //                 $page ++;
        //                 die(var_dump(json_encode(array('finish' => $finish, 'page' => $page, 'nb_pages' => $nb_pages, 'nb_artistes' => sizeof($moreMedia->data), 'nb_total_artistes' => $nb_total_artistes))));
        //                 die(json_encode(array('finish' => $finish, 'page' => $page, 'nb_pages' => $nb_pages, 'nb_artistes' => sizeof($moreMedia->data), 'nb_total_artistes' => $nb_total_artistes)));
        //             }
        //             $k++;
        //         } else {
        //             die(json_encode(array('finish' => 1, 'nb_pages' => $nb_pages)));
        //         }
        //         $moreMedia = $instagram->pagination($moreMedia);

        //     }
        //     die(json_encode(array('finish' => 1, 'nb_pages' => $nb_pages)));
            
        // }else {
        //     die(json_encode(array('finish' => 1, 'nb_pages' => $nb_pages)));
        // }
    }
}

function nni_get_code_success() {
    ?>
    <div class="notice notice-success is-dismissible">
        <p><?php _e( 'Done!', 'sample-text-domain' ); ?></p>
    </div>
    <?php
}
function nni_get_code_error() {
    ?>
    <div class="notice notice-error is-dismissible">
        <p><?php _e( 'Done!', 'sample-text-domain' ); ?></p>
    </div>
    <?php
}


/**
 * Liste des onglets dans lesquels seront rangés les champs de notre page d'options.
 *
 * @param array $tabs []
 * @return array $tabs Tableau des onglets : la clé d'une entrée est utilisée par le filtre chargeant les champs de l'onglet, la valeur d'une entrée est le titre de l'onglet.
 */
function options_set_tabs( $tabs ) {
	return [
		'filters'   => __( 'Filtres', 'nni-plugin' ),
		'general'  => __( 'API Instagram', 'nni-plugin' )
	];
}
add_filter( 'nni_plugin_options_tabs', __NAMESPACE__ . '\\options_set_tabs' );

/**
 * Ajoute des champs dans l'onglet "Général".
 *
 * @return array $fields Le tableau contenant nos champs.
 * @link https://carbonfields.net/docs/fields-usage/
 */
function options_general_tab_theme_fields() {
	$fields = [];

	$fields[] = Field::make( 'text', 'nni_instagram_app_id', __( "ID de l’app Instagram", 'nni-plugin' ) )
				->set_required();

	$fields[] = Field::make( 'text', 'nni_instagram_app_secret', __( "Clé secrète de l’app Instagram", 'nni-plugin' ) )
				->set_required();

	$fields[] = Field::make( 'text', 'nni_instagram_short_token', __( "Instagram Short Token (Ne pas modifier)", 'nni-plugin' ));
	$fields[] = Field::make( 'text', 'nni_instagram_long_token', __( "Instagram Long Token (Ne pas modifier)", 'nni-plugin' ));

	return $fields;
}
add_filter( 'nni_plugin_options_fields_tab_general', __NAMESPACE__ . '\\options_general_tab_theme_fields', 10 );

/**
 * Ajoute des champs dans l'onglet "Réseaux sociaux".
 *
 * @return array $fields Le tableau contenant nos champs.
 * @link https://carbonfields.net/docs/fields-usage/
 */
function options_filters_tab_theme_fields() {
	$fields = [];

	$fields[] = Field::make( 'textarea', 'nni_instagram_hashtags_filters', __( "Hashtags à récupérer", 'nni-plugin' ) )
                ->set_help_text( "Un hashtag par ligne sans #<br>Seules les publications Instagram comprenant au moins l'un de ces hashtags seront récupérées" );

    $fields[] = Field::make( 'rich_text', 'nni_instagram_no_results', __( "Message si aucun résultat", 'nni-plugin' ) );


    $fields[] = Field::make( 'complex', 'nni_filters_group', __( 'Groupes de filtres', 'nni-plugin' ) )
                    ->setup_labels( array(
                        'plural_name' => 'Groupes',
                        'singular_name' => 'Groupe',
                    ) )
                    ->add_fields( array(
                        Field::make( 'text', 'title', __( 'Titre du groupe', 'nni-plugin' ) )->set_width(50),
                        Field::make( 'complex', 'nni_filters', __( 'Filtres', 'nni-plugin' ) )
                        ->setup_labels( array(
                            'plural_name' => 'Filtres',
                            'singular_name' => 'Filtre',
                        ) )
                        ->add_fields( array(
                            Field::make( 'text', 'title', __( 'Titre', 'nni-plugin' ) )->set_width(50),
                            Field::make( 'text', 'hashtag', __( 'Hashtag', 'nni-plugin' ) )->set_width(50),
                        ) )
                    ) );
	return $fields;
}
add_filter( 'nni_plugin_options_fields_tab_filters', __NAMESPACE__ . '\\options_filters_tab_theme_fields', 10 );

function nni_get_images_html($nb = '', $data = false) {
    if(!$data) $data = new stdClass();
    $html = '';

    if(isset($data->title)) {
        ob_start();
        ?>
        <h3><?php echo $data->title; ?></h3>

        <?php if(isset($nb) && !empty(trim($nb))) : ?>
            <h6><?php echo$nb; ?></h6>
        <?php endif; ?>
        
        <!-- <?php if(isset($data->hashtags) && !empty(trim($data->hashtags))) : ?>
            <h5><?php echo $data->hashtags; ?></h5>
        <?php endif; ?> -->

        <?php if(isset($data->nni_artist_instagram) && !empty(trim($data->nni_artist_instagram))) : ?>
            <a href='https://www.instagram.com/<?php echo str_replace('https://www.instagram.com/', '', $data->nni_artist_instagram); ?>' target='_blank'>
                <img decoding="async" src="<?php echo plugin_dir_url( __FILE__ ) . 'img/instagram.svg'; ?>" alt="Instagram" class="nni_svg nni_svg_instagram">
                
            </a>
        <?php endif; ?>
        <?php if(isset($data->nni_artist_video) && !empty(trim($data->nni_artist_video))) : ?>
            <a href='<?php echo $data->nni_artist_video; ?>' target='_blank'>
                <img decoding="async" src="<?php echo plugin_dir_url( __FILE__ ) . 'img/video.svg'; ?>" alt="Bande démo" class="nni_svg nni_svg_video">
                
            </a>
        <?php endif; ?>
        <?php if(isset($data->nni_artist_facebook) && !empty(trim($data->nni_artist_facebook))) : ?>
            <a href='<?php echo $data->nni_artist_facebook; ?>' target='_blank'>
                <img decoding="async" src="<?php echo plugin_dir_url( __FILE__ ) . 'img/facebook.svg'; ?>" alt="Facebook" class="nni_svg nni_svg_facebook">
                
            </a>
        <?php endif; ?>
        <?php if(isset($data->nni_artist_linkedin) && !empty(trim($data->nni_artist_linkedin))) : ?>
            <a href='<?php echo $data->nni_artist_linkedin; ?>' target='_blank'>
                <img decoding="async" src="<?php echo plugin_dir_url( __FILE__ ) . 'img/linkedin.svg'; ?>" alt="LinkedIn" class="nni_svg nni_svg_linkedin">
                
            </a>
        <?php endif; ?>
        <?php if(isset($data->nni_artist_www) && !empty(trim($data->nni_artist_www))) : ?>
            <a href='<?php echo $data->nni_artist_www; ?>' target='_blank'>
                <img decoding="async" src="<?php echo plugin_dir_url( __FILE__ ) . 'img/www.svg'; ?>" alt="Site Internet" class="nni_svg nni_svg_www">
                
            </a>
        <?php endif; ?>
    
        <?php
        $html = ob_get_contents();
        ob_end_clean();

    }
    $html = str_replace(PHP_EOL, '', $html);
    $html = str_replace(CHR(10),"",$html);
    $html = str_replace(CHR(13),"",$html);
    $html = str_replace('"', '&quot;', $html);
    return trim($html);
}

function nni_get_images_data($image_id = 0, $data = false) {
    $output = array();
    // die(var_dump($data));

    if(isset($image_id) && $image_id > 0) {
        $lazy = wp_get_attachment_image_src($image_id, 'nni_lazy');
        $small = wp_get_attachment_image_src($image_id, 'medium');
        $large = wp_get_attachment_image_src($image_id, 'nni_large');
        if(isset($lazy[0]) && !empty($lazy[0]) && isset($small[0]) && !empty($small[0]) && isset($large[0]) && !empty($large[0])) {
            $output = array(
                'lazy' => $lazy[0],
                'small' => $small[0],
                'width' => $small[1],
                'height' => $small[2],
                'large' => $large[0]
            );
        }
    }
    return $output;
}

function nni_get_artists($cache = true) {
    $output = [];
    $artists = false;
    // $cache = false;
    if($cache) {
        $nb_pages = intval(get_transient('nni_get_artists_nb_pages'));
        for($i = 0 ; $i < $nb_pages ; $i ++) {
            $key = 'nni_get_artists_page_'.$i;
            $tmp = unserialize(get_transient($key));
            // if($i < 2) var_dump($key, $tmp);
            // echo '<br><hr><br>';
            if(is_object($tmp)) {
                // var_dump($key, $tmp);
                $output[] = $tmp;
            }
        }
        // die(var_dump($nb_pages, $output));
        // $output = json_decode(get_transient('nni_get_artists'));
    }
    
    
    if(!$output || (is_array($output) && sizeof($output) == 0)) {        
        $artists = get_posts(array(
            'numberposts' => -1,
            'post_type' => 'nni_artist'
        ));
        global $nni_artists_fields;
        
        // $filters = carbon_get_theme_option('nni_filters');
        // $in_filters = [];
        // foreach($filters as $filter) :
        //     if(!empty(trim($filter['hashtag']))) $in_filters[] = trim($filter['hashtag']);
        // endforeach;
        
        foreach($artists as $k=>$artist) {
            $tmp = new stdClass();
            $tmp->id = $artist->ID;
            $tmp->title = trim(strstr($artist->post_title, ',', true));
            
            foreach($nni_artists_fields as $field) {
                $tmp->{$field} = carbon_get_post_meta($artist->ID, $field);
            }
            // $hashtags = explode(' ', $tmp->nni_artist_instagram_hashtags);
            // foreach($hashtags as $kh=>$hashtag) {
            //     if(in_array(trim($hashtag), $in_filters)) {

            //     }
            // }
            if(!empty(trim($tmp->nni_artist_instagram_hashtags))) {
                // $tmp->hashtags = '#'.str_replace(' ', ' #', trim($tmp->nni_artist_instagram_hashtags));
                $tmp->nni_artist_instagram_hashtags = 'nni_filter_'.str_replace(' ', ' nni_filter_', trim($tmp->nni_artist_instagram_hashtags));
            } else {
                // $tmp->hashtags = '';
                $tmp->nni_artist_instagram_hashtags = '';
            }
            
            $tmp->images = array();
            $image_id = get_post_thumbnail_id($artist->ID);
            $image_data = nni_get_images_data($image_id, $tmp);
            if(sizeof($image_data) > 0) {
                $tmp->images[] = $image_data;
            }
            // $other_images = carbon_get_post_meta( $artist->ID, 'nni_artist_instagram_other_images');
            // if(is_array($other_images) && sizeof($other_images) > 0) {
            //     foreach($other_images as $other_image) {
            //         if(isset($other_image['image'])) {
            //             $image_data = nni_get_images_data($other_image['image'], $tmp);
            //             if(sizeof($image_data) > 0) {
            //                 $tmp->images[] = $image_data;
            //             }
            //         }
            //     }
            // }
            if(sizeof($tmp->images) > 0) {
                foreach($tmp->images as $ki=>$image) {
                    $nb = (sizeof($tmp->images) > 1) ? 'Photo '.($ki + 1).'/'.sizeof($tmp->images) : '';

                    $tmp->images[$ki]['html'] = nni_get_images_html($nb, $tmp);                    
                }
                $output[] = $tmp;
            }
        }
        
        // $output_split = array_chunk($output, 1, true);
        $nb_pages = sizeof($output);
        nni_delete_transients_with_prefix('nni_get_artists_');
        set_transient('nni_get_artists_nb_pages', $nb_pages);
        $cpt = 0;
        foreach($output as $o) {
            delete_transient('nni_get_artists_page_'.$cpt);
            set_transient('nni_get_artists_page_'.$cpt, serialize($o));
            
            $cpt ++;
        }
        
        // set_transient('nni_get_artists', json_encode($output));
    }
    // shuffle($output);
    return $output;
}

/**
 * Delete all transients from the database whose keys have a specific prefix.
 *
 * @param string $prefix The prefix. Example: 'my_cool_transient_'.
 */
function nni_delete_transients_with_prefix( $prefix ) {
	$transients = nni_get_transient_keys_with_prefix( $prefix );
    foreach ( $transients as $key ) {
		delete_transient( $key );
	}
}

/**
 * Gets all transient keys in the database with a specific prefix.
 *
 * Note that this doesn't work for sites that use a persistent object
 * cache, since in that case, transients are stored in memory.
 *
 * @param  string $prefix Prefix to search for.
 * @return array          Transient keys with prefix, or empty array on error.
 */
function nni_get_transient_keys_with_prefix( $prefix ) {
	global $wpdb;


	$prefix = $wpdb->esc_like( '_transient_' . $prefix );
	$sql    = "SELECT `option_name` FROM $wpdb->options WHERE `option_name` LIKE '%s'";
	$keys   = $wpdb->get_results( $wpdb->prepare( $sql, $prefix . '%' ), ARRAY_A );

	if ( is_wp_error( $keys ) ) {
		return [];
	}

	return array_map( function( $key ) {
		// Remove '_transient_' from the option name.
		// return ltrim( $key['option_name'], '_transient_' );
		return str_replace( '_transient_nni_', 'nni_', $key['option_name']);
	}, $keys );
}

function nni_update_artists($media, $existing_data) {

    $output = '';
    if(is_array($media)) {
        foreach($media as $cpt_m=>$m) {
            
                    
            // Create post object
            if(isset($m->id) && $m->id > 0) {

                // Filtre hastags
                $nni_instagram_hashtags_filters = carbon_get_theme_option('nni_instagram_hashtags_filters');
                $filters_ex = explode(PHP_EOL, $nni_instagram_hashtags_filters);
                $filters = array();
                
                foreach($filters_ex as $filter) {
                    $filter = str_replace('#', '', trim($filter));
                    if(!empty($filter)) {
                        $filters[] = $filter;
                    }
                }
                $do = false;
                if(sizeof($filters) > 0) {
                    foreach($filters as $filter) {
                        if(strpos($m->caption, '#'.$filter) !== false) {
                            $do = true;
                            break;
                        }
                    }
                } else {
                    $do = true;
                }

                if($do) {
                    //--
                    $instagram_profil = '';
                    if(isset($m->caption)) {
                        $caption = str_replace(PHP_EOL, ' ', $m->caption);
                        preg_match_all('/@([\w._-]+)/', $caption, $matches);
                        foreach ($matches[1] as $match) {
                            if(!empty($match)) {
                                $instagram_profil = $match;
                                break;
                            }
                        }
                    }
                    
                    $hashtags = array();
                    if(isset($m->caption)) {
                        $caption = str_replace(PHP_EOL, ' ', $m->caption);
                        $title = wp_strip_all_tags( $caption );
                        $title = preg_replace('/#\w+\h*/', '', $title);
                        preg_match_all('/#(\w+)/', $caption, $matches);
                        foreach ($matches[1] as $match) {
                            $hashtags[] = $match;
                        }
                    } else {
                        $title = $m->id;
                    }

                    $post_id = 0;
                    $new_post = false;
                    if(isset($existing_data[$m->id]) && $existing_data[$m->id] > 0) {
                        $post_id = $existing_data[$m->id];
                        wp_update_post( array('ID' => $post_id, 'post_title' => $title) );
                        $output = 'UPDATE '.$post_id;
                    } else {
                        
                        if(isset($m->media_type) && in_array($m->media_type, ['IMAGE' , 'CAROUSEL_ALBUM'])) {
                            $new_post = array(
                                'post_title'    => $title,
                                'post_status'   => 'publish',
                                'post_type'     => 'nni_artist',
                                'post_date'     => date("Y-m-d H:i:s", strtotime($m->timestamp))
                            );
                            $post_id = wp_insert_post( $new_post );
                            $new_post = true;
                            $output = 'NEW '.$post_id;
                        }
                    }

                    if($post_id > 0) {

                        $timestamp = carbon_get_post_meta( $post_id, 'nni_artist_instagram_timestamp');
                        // $other_images = carbon_get_post_meta( $post_id, 'nni_artist_instagram_other_images');
                        
                        $json = $m;
                        
                        $do = false;
                        if(empty($timestamp) || $m->timestamp > $timestamp) {
                            $do = true;
                        }         
                        
                        /*if(isset($json->media_type) && in_array($json->media_type, ['IMAGE' , 'CAROUSEL_ALBUM'])) {
                            $image_url = carbon_get_post_meta( $post_id, 'nni_artist_instagram_image_url' );
                            // $other_images = carbon_get_post_meta( $post_id, 'nni_artist_instagram_other_images' );
                            if($image_url != $json->media_url) {
                                $do = true;
                                // die(var_dump($do,$post_id, $image_url, $json->media_url));
                            }
                            //  else {
                            //     if(isset($json->children->data) && is_array($json->children->data) && sizeof($json->children->data) > 0) {
                            //         foreach($json->children->data as $k=>$children_data) {
                            //             if($k > 0) {
                            //                 $media_url = $children_data->media_url;
                            //                 if(isset($other_images[$k-1]['instagram_url'])) {
                            //                     if($other_images[$k-1]['instagram_url'] != $children_data->media_url) {
                            //                         $do = true;
                            //                     }
                            //                 }else {
                            //                     $do = true;
                            //                 }
                            //             }
                            //         }
                            //     }
                            // }
                        }*/
                        if($new_post) {
                            $do = true;
                        }
                        if($do) {
                            // die(var_dump($do));
                        }
                        carbon_set_post_meta( $post_id, 'nni_artist_instagram_id', $m->id );
                        carbon_set_post_meta( $post_id, 'nni_artist_instagram', $instagram_profil );
                        carbon_set_post_meta( $post_id, 'nni_artist_instagram_permalink', $m->permalink );
                        carbon_set_post_meta( $post_id, 'nni_artist_instagram_timestamp', $m->timestamp );
                        carbon_set_post_meta( $post_id, 'nni_artist_instagram_json', json_encode($m) );
                        carbon_set_post_meta( $post_id, 'nni_artist_instagram_hashtags', implode(' ', $hashtags) );
                        carbon_set_post_meta( $post_id, 'nni_artist_instagram_image_url', $m->media_url );
                        require_once(ABSPATH . 'wp-admin/includes/media.php');
                        require_once(ABSPATH . 'wp-admin/includes/file.php');
                        require_once(ABSPATH . 'wp-admin/includes/image.php');
                        if($do) {           
                            if(isset($json->media_type)) {
                                // if($json->media_type == 'IMAGE') { 

                                    // Convers HEIC TO JPG
                                    $image_id = 0;
                                    if(strpos($json->media_url, '.heic') !== false) {
                                        // URL de l'image au format HEIC
                                        $image_url = $json->media_url;
                
                                        // Chemin temporaire pour l'image convertie
                                        // $temp_file_heic = tempnam(sys_get_temp_dir(), 'converted_image_').'.heic';
                                        $temp_file = 'converted_image_'.uniqid().'.jpg';

                                        $file_content = file_get_contents($image_url);

                                        // Sauvegarder le contenu du fichier dans un fichier local
                                        file_put_contents(WP_CONTENT_DIR . '/uploads/tmp/'.$temp_file, $file_content);

                                        $upload_dir_info = wp_upload_dir();

                                        // URL du répertoire "uploads"
                                        $uploads_url = $upload_dir_info['baseurl'].'/tmp/'.$temp_file;

                
                                        // Utiliser media_sideload_image pour importer l'image convertie
                                        $image_id = media_sideload_image( $uploads_url, $post_id, $post_id, 'id');
                                        // die(var_dump('HEIC ssd', $image_id, $uploads_url, $json->media_url, $temp_file, $temp_file_heic));
                                        

                                    } else {
                                        // die(var_dump('NO HEIC', $json->media_url));
                                        $image_id = media_sideload_image( $json->media_url, $post_id, $post_id, 'id');
                                    }

                                    if($image_id > 0) {
                                        set_post_thumbnail($post_id, $image_id);
                                    }
                                // }
                                // if($json->media_type == 'CAROUSEL_ALBUM') { 
                                //     if(isset($json->children->data) && is_array($json->children->data) && sizeof($json->children->data) > 0) {
                                //         // $other_images = array();
                                //         foreach($json->children->data as $k=>$children_data) {
                                //             if($k == 0) {
                                //                 $image_id = media_sideload_image( $json->media_url, 0, '', 'id');
                                //                 if($image_id > 0) {
                                //                     set_post_thumbnail($post_id, $image_id);
                                //                 }
                                //             }else {
                                //                 if(isset($children_data->media_type) && $children_data->media_type == 'IMAGE' && isset($children_data->media_url)) {
                                //                     $image_id = media_sideload_image( $children_data->media_url, 0, '', 'id');
                                //                     if($image_id > 0) {
                                //                         $other_images[] = array('_type' => '_', 'image' => $image_id, 'instagram_url' => $children_data->media_url);
                                //                     }
                                //                 }
                                //             }
                                //         }
                                //         if(sizeof($other_images) > 0) {
                                //             carbon_set_post_meta( $post_id, 'nni_artist_instagram_other_images', $other_images );
                                //         }
                                //     }
                                // }
                            }
                        }
                    }
                }
            }
        }
    }
    return $output;
}


/**
 * Affiche la valeur d'un champ sous la metabox d'onglets
 *
 * @return void
 */
function display_content_after_fields() {
    
        ?>
        <?php
        // if(!is_null($media->data)) {
        //     nni_update_artists($media, $existing_data, $instagram);
        // }

}
add_action( 'carbon_fields_container_options_du_plugin_after_fields', __NAMESPACE__ . '\\display_content_after_fields' );

/**
 * Affiche un contenu promotionnel dans la sidebar
 *
 * @return void
 */
function display_content_after_sidebar() {
	?>
	<style>
		#nni-plugin-promo-box {
			background: #fff;
			border:1px solid #DADADA;
			padding:2rem;
			text-align: center;
			border-radius:5px;
		}

		#nni-plugin-promo-box h2 {
			font-size: 3rem;
			margin:0;
		}
	</style>
	<div id="nni-plugin-promo-box" class="wp-core-ui">
<?php

$instagram = new InstagramBasicDisplay([
    'appId' => \carbon_get_theme_option( 'nni_instagram_app_id' ),
    'appSecret' => \carbon_get_theme_option( 'nni_instagram_app_secret' ),
    'redirectUri' => admin_url()
]);

echo "<p><a href='{$instagram->getLoginUrl()}' class='button button-info button-small'>Récupérer le token</a></p>";


$token = \carbon_get_theme_option('nni_instagram_long_token');
if(!empty($token)) {
    $instagram->setAccessToken($token);
    // $instagram->setMediaFields('caption, id, media_type, media_url, timestamp, permalink, children{media_type, media_url, timestamp, thumbnail_url}');
    $instagram->setMediaFields('caption, id, media_type, media_url, timestamp, permalink');


    $all_data = $args = get_posts( array(
        'numberposts' => -1,
        'post_type'   => 'nni_artist'
    ) );
    $existing_data = [];
    foreach($all_data as $d) {
        $i_id = carbon_get_post_meta( $d->ID, 'nni_artist_instagram_id' );
        if($i_id > 0) {
            $existing_data[$i_id] = $d->ID;
        }
    }

    $media = $instagram->getUserMedia();
}
    ?>
<hr><p><a id="nni-refresh-link" href='<?php echo admin_url('admin.php?page=plugin-nni&refresh=1'); ?>' class='button button-info button-large'>Vider le cache</a></p>
        <hr><p><a id="nni-sync-link" href='<?php echo admin_url('admin.php?page=plugin-nni&sync=1'); ?>' class='button button-primary button-large'>Lancer la synchronisation</a></p>

        <p class="nni_loader" style="display: none;"><img src="/wp-admin/images/loading.gif"> Chargement <span class="nni_page">1</span> / <span class="nni_nb_pages"></span></p>
        <p class="nni_finish" style="display: none;">Synchronisation terminée</p>

        <script>
            $ = jQuery;

            $(document).on( 'click', '#nni-sync-link', function( event ) {
                event.preventDefault();
                nni_ajax_sync($(this).attr('href'), 1);
            });
            function nni_ajax_sync(url, page) {

                $('.nni_loader').show();
                $('.nni_finish').hide();
                
                $.ajax({
                    url : url,
                    type : 'POST',
                    dataType : 'json',
                    data: 'page='+page,
                    success : function(json, statut){
                        if(json.finish == '0') {
                            $('.nni_page').html(json.page);
                            $('.nni_nb_pages').html(json.nb_pages);
                            nni_ajax_sync(url, json.page);
                        } else {
                            $('.nni_loader').hide();
                            $('.nni_finish').show();
                        }
                        if (typeof json.error !== 'undefined') {
                            $('.nni_loader').hide();
                            $('.nni_finish').show();
                            alert(json.error);
                        }
                    },

                    error : function(resultat, statut, erreur){
                        $('.nni_loader').hide();
                        $('.nni_finish').show();
                        alert('error');
                    }
                });
            }

        </script>
		<!-- <h2>🚀</h2>
		<h3>Passez à la version pro&nbsp;!</h3>
		<p><strong>Pour seulement 899€ HT par mois, bénéficiez de nombreuses améliorations :</strong></p>
		<ul>
			<li>✔ Super fonctionnalité 1</li>
			<li>✔ Médiocre fonctionnalité 2</li>
			<li>✔ Fonctionnalité passable 3</li>
		</ul> -->
	</div>
	<?php
}
add_action( 'carbon_fields_container_options_du_plugin_after_sidebar', __NAMESPACE__ . '\\display_content_after_sidebar' );




function nni_shortcode_annuaire(){
    $artists = nni_get_artists(true);
    ob_start();
?>

<div class="nni_filters">

    <?php
        $filters_groups = carbon_get_theme_option('nni_filters_group');
        // echo '<pre>';
        // var_dump($filters_groups);
        // echo '</pre>';
        foreach($filters_groups as $filters_group) :
            if(isset($filters_group['title']) && isset($filters_group['nni_filters']) && is_array($filters_group['nni_filters']) && sizeof($filters_group['nni_filters']) > 0) :
                ?>

            <div class="nni_filter_group">
                <h3 data-target="#nni_filter_group_<?php echo sanitize_title($filters_group['title']); ?>"><?php echo $filters_group['title']; ?> 
                    <img class="nni_toggle_filter nni_toggle_opened" src="<?php echo plugin_dir_url( __FILE__ ) . 'img/arrow-right.svg'; ?>">
                    <img class="nni_toggle_filter nni_toggle_closed" src="<?php echo plugin_dir_url( __FILE__ ) . 'img/arrow-down.svg'; ?>">
                </h3>
                <div class="nni_filter_group_filters" id="nni_filter_group_<?php echo sanitize_title($filters_group['title']); ?>">
                    <?php
                        foreach($filters_group['nni_filters'] as $filter) :
                            ?>
                            <div class="nni_button" data-filter-group="<?php echo sanitize_title($filters_group['title']); ?>" data-filter=".nni_filter_<?php echo $filter['hashtag']; ?>"><?php echo $filter['title']; ?></div>
                            <?php
                        endforeach;
                    ?>
                </div>
            </div>
            <?php
            endif;
        endforeach;
    ?>
</div>
<script>
    var nni_filters_groups = [];
    var nni_filters = [];
        <?php
        $filters_groups = carbon_get_theme_option('nni_filters_group');
        foreach($filters_groups as $filters_group) :
            if(isset($filters_group['title']) && isset($filters_group['nni_filters']) && is_array($filters_group['nni_filters']) && sizeof($filters_group['nni_filters']) > 0) :
                ?>
    nni_filters_groups.push('<?php echo sanitize_title($filters_group['title']); ?>');
    nni_filters['<?php echo sanitize_title($filters_group['title']); ?>'] = [];
                <?php
                foreach($filters_group['nni_filters'] as $filter) :
                    
                    ?>
    nni_filters['<?php echo sanitize_title($filters_group['title']); ?>'].push('.nni_filter_<?php echo $filter['hashtag']; ?>');
    <?php
                        endforeach;
            endif;
        endforeach;
    ?>
</script>

<div class="nni_grid" id="nni_lightgallery">
    <div class="nni_loader">
        <img src="<?php echo plugin_dir_url( __FILE__ ) . 'img/loader.webp'; ?>">
    </div>
<?php    
    foreach($artists as $k=>$artist) :
        ?>
        <a data-sub-html="<?php echo $artist->images[0]['html']; ?>" class="nni_artist nni_selected <?php echo $artist->nni_artist_instagram_hashtags; ?>" href="<?php echo $artist->images[0]['large']; ?>" data-lightbox="image-1" data-title="">
            <img data-index="<?php echo $k; ?>" id="<?php echo $artist->nni_artist_instagram_id; ?>" src="<?php echo $artist->images[0]['lazy']; ?>" data-src="<?php echo $artist->images[0]['small']; ?>" width="<?php echo $artist->images[0]['width']; ?>" height="<?php echo $artist->images[0]['height']; ?>" class="nni_image <?php echo ($artist->images[0]['width'] > $artist->images[0]['height']) ? 'nni_image_landscape' : 'nni_image_portrait'; ?>" >
        </a>
        <?php 
        /*if(sizeof($artist->images) > 1) : 
            foreach($artist->images as $ki=>$image) : 
                if($ki > 0) :
            ?>
            <a data-sub-html="<?php echo $image['html']; ?>" class="nni_artist_gallery <?php echo $artist->nni_artist_instagram_hashtags; ?>" href="<?php echo $image['large']; ?>" data-lightbox="image-1" data-title="My caption">
                <img src="<?php echo $image['lazy']; ?>" data-src="<?php echo $image['small']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" class="nni_image <?php echo ($artist->images[0]['width'] > $artist->images[0]['height']) ? 'nni_image_landscape' : 'nni_image_portrait'; ?>" >
            </a>
            <?php
                endif;
            endforeach;
        endif;*/ ?>

        
        <?php
    endforeach;
?>
</div>
<div class="nni_no_results" style="<?php if(sizeof($artists) > 0) echo 'display:none;'; ?>"><?php echo carbon_get_theme_option('nni_instagram_no_results'); ?></div>

<?php


    // END

    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}
add_shortcode('nni_annuaire',  __NAMESPACE__ . '\\nni_shortcode_annuaire');

function nni_check_function() {
    global $post;

    if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'nni_annuaire') ) {
        // if ( !is_admin() ) wp_deregister_script('jquery');

        $version = '1.1.63';//.time();
        wp_enqueue_style( 'nni-styles', plugin_dir_url( __FILE__ ) . 'css/nni_annuaire.css', [], $version);
        // wp_enqueue_style( 'nni-lightbox', plugin_dir_url( __FILE__ ) . 'lightbox/css/lightbox.min.css', [], '1.0.1' );
        wp_enqueue_style( 'nni-lightgallery', plugin_dir_url( __FILE__ ) . 'lightgallery/css/lightgallery.min.css', [], '1.0.1' );
        

        wp_register_script( 'nni-imagesloaded', plugin_dir_url( __FILE__ ) . 'js/imagesloaded.pkgd.min.js', array( 'jquery' ), '1.2', true );
        wp_enqueue_script( 'nni-imagesloaded' ); 
        wp_register_script( 'nni-isotope', plugin_dir_url( __FILE__ ) . 'js/isotope.pkgd.min.js', array( 'jquery' ), '1.1', true );
        wp_enqueue_script( 'nni-isotope' );
        // wp_register_script( 'nni-masonry', plugin_dir_url( __FILE__ ) . 'js/masonry.pkgd.min.js', array( 'jquery' ), '1.1', true );
        // wp_enqueue_script( 'nni-masonry' );
        // wp_register_script( 'nni-infinite', plugin_dir_url( __FILE__ ) . 'js/infinite-scroll.pkgd.min.js', array( 'jquery' ), '1.1', true );
        // wp_enqueue_script( 'nni-infinite' );
        wp_register_script( 'nni-lazyload', plugin_dir_url( __FILE__ ) . 'js/lazyload.js', array( 'jquery' ), '1.1', true );
        wp_enqueue_script( 'nni-lazyload' );
        wp_register_script( 'nni-lightgallery', plugin_dir_url( __FILE__ ) . 'lightgallery/js/lightgallery.js', array( 'jquery' ), '1.2', true );
        wp_enqueue_script( 'nni-lightgallery' ); 
        // wp_register_script( 'nni-lightbox', plugin_dir_url( __FILE__ ) . 'lightbox/js/lightbox.min.js', array( 'jquery' ), '1.1', true );
        // wp_enqueue_script( 'nni-lightbox' );
        wp_register_script( 'nni-annuaire', plugin_dir_url( __FILE__ ) . 'js/nni-annuaire.js', array( 'nni-isotope' ), $version, true );
        wp_enqueue_script( 'nni-annuaire' );
        wp_register_script( 'nni-lightgallery-hash', plugin_dir_url( __FILE__ ) . 'lightgallery/js/lg-hash-custom.js', array( 'jquery' ), '1.3'.$version, true );
        wp_enqueue_script( 'nni-lightgallery-hash' );
    }
}

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\nni_check_function', 999);

function nni_remove_scripts() {
    global $post;

    if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'nni_annuaire') ) {
        global $wp_scripts;
        // die(var_dump($wp_scripts));
        wp_deregister_script('themify-main-script-js');
        wp_dequeue_script('themify-main-script-js');
    }
}
add_action('wp_print_scripts', __NAMESPACE__ . '\\nni_remove_scripts', 999);

function nni_disable_plugin_updates( $value ) {
    if ( isset($value) && is_object($value) ) {
      if ( isset( $value->response['github-updater/github-updater.php'] ) ) {
        unset( $value->response['github-updater/github-updater.php'] );
      }
    }
    if ( isset($value) && is_object($value) ) {
      if ( isset( $value->response['github-updater/git-updater.php'] ) ) {
        unset( $value->response['github-updater/git-updater.php'] );
      }
    }
    return $value;
  }
add_filter( 'site_transient_update_plugins', __NAMESPACE__ . '\\nni_disable_plugin_updates' );


function nni_refresh_cache_artists( $post_id, $post, $update )  {

	// Sinon la fonction se lance dès le clic sur "ajouter"
	if( ! $update ) {
    	return;
	}

	// On ne veut pas executer le code lorsque c'est une révision
	if( wp_is_post_revision( $post_id ) ) {
		return;
	}

	// On évite les sauvegardes automatiques
	if( defined( 'DOING_AUTOSAVE' ) and DOING_AUTOSAVE ) {
		return;
	}

	// Seulement pour les articles
	if( $post->post_type != 'nni_artist' ) {
    	return;
	}
    wp_schedule_single_event( time() + 10, 'nni_refresh_cache_artists_command');


}
add_action( 'save_post',  __NAMESPACE__ . '\\nni_refresh_cache_artists', 10, 3 );

function nni_refresh_cache_artists_command() {    
    nni_get_artists(false);
}

add_action( 'nni_refresh_cache_artists_command', __NAMESPACE__ . '\\nni_refresh_cache_artists_command', 10, 3 );




function nni_register_daily_instagram_api_sync() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'nni_cron_instagram_api_sync' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'nni_cron_instagram_api_sync' );
    }
}
add_action( 'init', __NAMESPACE__ . '\\nni_register_daily_instagram_api_sync');



function nni_cron_instagram_api_sync_command() {
    nni_start_sync(1, true);    
}
add_action( 'nni_cron_instagram_api_sync', __NAMESPACE__ . '\\nni_cron_instagram_api_sync_command' );

function nni_cron_instagram_api_sync_next_command() {
    $page = intval(get_transient('nni_instagram_current_page_api_json'));
    if($page > 1) {
        nni_start_sync($page, true);    
    }
}
add_action( 'nni_cron_instagram_api_sync_next_command', __NAMESPACE__ . '\\nni_cron_instagram_api_sync_next_command' );