$ = jQuery;
    


    // var nni_lightgallery = document.getElementById('nni_lightgallery');
    // nni_lightgallery.addEventListener('onBeforeOpen', () => {
    //     $('.nni_artist_other').show();
    // });
    // nni_lightgallery.addEventListener('onBeforeClose', () => {
    //     $('.nni_artist_other').hide();
    // });
    // $(document).ready(function() {
        const lg = $('#nni_lightgallery').lightGallery({
            fullscreen: false,
            download: false,
            hash: true
        }); 
        // console.log($('#nni_lightgallery').data('lightGallery').openGallery(0));

    $('.nni_grid').imagesLoaded( function() {

        // init Isotope
        var $grid = $('.nni_grid').isotope({
            itemSelector: '.nni_artist',
            masonry: {
            columnWidth: 0,
            gutter: 0
            }
        });
        $('.nni_loader').hide();
        $('.nni_image').css('opacity', 1);
    });
    $(document).ready(function() {
        _hash = window.location.hash;
        
        var _idx = _hash.split('id=')[1];
        if(_idx > 0) {
            $("#"+_idx).trigger('click');
        }
    });
  
    

  
// store filter for each group
var filters = [];

$('.nni_filter_group').on( 'click', 'h3', function( event ) {

    $($(this).data('target')).closest('.nni_filter_group').toggleClass('nni_open');
});

$(window).resize(function() {
    setTimeout(function(){
        var isotopeFilters = formatFilters();
        $('.nni_grid').isotope({ filter: isotopeFilters });
    }, 200);
});

// change is-checked class on buttons
$('.nni_filters').on( 'click', '.nni_button', function( event ) {
    
    var $target = $( event.currentTarget );
    $target.toggleClass('is-checked');
    var isChecked = $target.hasClass('is-checked');
    var filter = $target.attr('data-filter');
    var filterGroup = $target.attr('data-filter-group');
    if ( isChecked ) {
        addFilter( filter, filterGroup);
    } else {
        removeFilter( filter, filterGroup );
    }
    // filter isotope
    // group filters together, inclusive
    //   alert(filters.join(''));
    // $grid.isotope({ filter: filters.join('') });
    var isotopeFilters = formatFilters();
    $('.nni_grid').isotope({ filter: isotopeFilters });
    
    var visibleItemsCount = $('.nni_grid').data('isotope').filteredItems.length;
    if( visibleItemsCount > 0 ){
        $('.nni_no_results').hide();
    }
    else{
        $('.nni_no_results').show();
    }
    $('#nni_lightgallery').data('lightGallery').destroy(true);
    // window.lgData[document.getElementById('nni_lightgallery').getAttribute('lg-uid')].destroy(true);
    $('#nni_lightgallery').lightGallery({
        fullscreen: false,
        download: false,
        hash: true,
        selector: isotopeFilters
    }); 
    // $('html, body').animate({
    //     scrollTop: $('#nni_lightgallery').offset().top
    //   }, 800, function(){
    // });
    // nni_lightgallery.addEventListener('onBeforeOpen', () => {
    //     alert('onBeforeOpen');
    //     $('.nni_artist_other').show();
    // });
    // nni_lightgallery.addEventListener('onBeforeClose', () => {
    //     alert('onBeforeClose');
    //     $('.nni_artist_other').hide();
    // });

});
  
function addFilter( filter ) {
    if ( filters.indexOf( filter ) == -1 ) {
      filters.push( filter );
    }
}

function removeFilter( filter ) {
    var index = filters.indexOf( filter);
    if ( index != -1 ) {
        filters.splice( index, 1 );
    }
}

function formatFilters() {
    // console.log('START formatFilters *');
    var output = '.nni_artist.nni_selected';
    var nni_filters_tmp = [];
    $( ".nni_artist" ).each(function() {
        $( this ).addClass( "nni_selected" );
    });
    for (var i = 0; i < nni_filters_groups.length; i++) {
        nni_filters_tmp[nni_filters_groups[i]] = [];
    }

    for (var i = 0; i < nni_filters_groups.length; i++) {
        if (typeof nni_filters[nni_filters_groups[i]] != 'undefined' && nni_filters[nni_filters_groups[i]].length > 0) {
            for (var k = 0; k < filters.length; k++) {
                var index = nni_filters[nni_filters_groups[i]].indexOf( filters[k]);
                // console.log(nni_filters[nni_filters_groups[i]]);
                // console.log(filters[k]);
                // console.log(index);
                if ( index != -1 ) {
                    // console.log('remove');
                    // console.log(nni_filters_groups[i]);
                    // console.log(index);
                    nni_filters_tmp[nni_filters_groups[i]].push( filters[k] );
                }
            }
        }
    }
    
    $( ".nni_artist" ).each(function() {
        //$( this ).addClass( "foo" );
        var selected = true;
        for (var i = 0; i < nni_filters_groups.length; i++) {
            if (typeof nni_filters_tmp[nni_filters_groups[i]] != 'undefined' && nni_filters_tmp[nni_filters_groups[i]].length > 0) {
                var tmp = false;
                for (var k = 0; k < nni_filters_tmp[nni_filters_groups[i]].length; k++) {
                    var classSearch = nni_filters_tmp[nni_filters_groups[i]][k].replace('.', '');
                    // console.log(classSearch);
                    if($(this).hasClass(classSearch)) {
                        tmp = true;
                    }
                }
                if(!tmp) {
                    selected = false;
                }
            }
        }
        if(!selected) {
            $( this ).removeClass( "nni_selected" );
        }
    });
    
    // console.log(nni_filters_groups);
    // console.log(nni_filters);
    // console.log('**');
    // console.log(nni_filters_tmp);
    // console.log('**');
    // console.log(filters);
    // console.log(output);
    // console.log('END');
    return output;
}

$(document).ready(function() {
    // Lazy Load
    let images = document.querySelectorAll(".nni_image");
    new lazyload(images, {
    });
});
