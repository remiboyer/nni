/*! lg-hash - v1.0.4 - 2017-12-20
* http://sachinchoolur.github.io/lightGallery
* Copyright (c) 2017 Sachin N; Licensed GPLv3 */

(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
      // AMD. Register as an anonymous module unless amdModuleId is set
      define(['jquery'], function (a0) {
        return (factory(a0));
      });
    } else if (typeof exports === 'object') {
      // Node. Does not work with strict CommonJS, but
      // only CommonJS-like environments that support module.exports,
      // like Node.
      module.exports = factory(require('jquery'));
    } else {
      factory(jQuery);
    }
  }(this, function ($) {
  
  (function() {
  
      'use strict';
  
      var defaults = {
          hash: true
      };
  
      var Hash = function(element) {
  
          this.core = $(element).data('lightGallery');
  
          this.core.s = $.extend({}, defaults, this.core.s);
  
          if (this.core.s.hash) {
              this.oldHash = window.location.hash;
              this.init();
          }
  
          return this;
      };
  
      Hash.prototype.init = function() {
          var _this = this;
          var _hash;
  
          // Change hash value on after each slide transition
          _this.core.$el.on('onAfterSlide.lg.tm', function(event, prevIndex, index) {
            var main = document.getElementById('nni_lightgallery');
            var child = main.querySelectorAll('.nni_selected .nni_image')[index];
            if(child.id > 0) {
                window.location.hash = 'id=' + child.id;
            }
          });
  
          // Listen hash change and change the slide according to slide value
          $(window).on('hashchange.lg.hash', function() {

              
            _hash = window.location.hash;
            
            var _idx = _hash.split('id=')[1];
            var iii = $( "#nni_lightgallery .nni_selected .nni_image" ).index( $("#"+_idx) );
            
            if(iii > -1 && _this.core.index != iii) {
                _this.core.slide(iii, false, false);
            }
          });
      };
  
      Hash.prototype.destroy = function() {

        var doc = document.documentElement;
        var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
        var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
        window.location.hash = '';
        $(window).scrollTop( top );
        
        return;
          if (!this.core.s.hash) {
              return;
          }
  
          // Reset to old hash value
          if (this.oldHash && this.oldHash.indexOf('lg=' + this.core.s.galleryId) < 0) {
              if (history.replaceState) {
                  history.replaceState(null, null, this.oldHash);
              } else {
                  window.location.hash = this.oldHash;
              }
          } else {
              if (history.replaceState) {
                  history.replaceState(null, document.title, window.location.pathname + window.location.search);
              } else {
                  window.location.hash = '';
              }
          }
  
          this.core.$el.off('.lg.hash');
  
      };
  
      $.fn.lightGallery.modules.hash = Hash;
  
  })();
  
  
  }));
  