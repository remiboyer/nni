<?php

function nni_shortcode_annuaire(){
    $artists = nni_get_artists(false);
    ob_start();
?>

<h2>Annuaire !</h2>

<div class="filters">

  <div class="ui-group">
    <div class="button-group js-radio-button-group">
<?php

    $filters = carbon_get_theme_option('nni_filters');
    // echo '<pre>';
    // var_dump($filters);
    // echo '</pre>';
    foreach($filters as $filter) :
        ?>
        <div class="nni_button" data-filter=".<?php echo $filter['hashtag']; ?>"><?php echo $filter['title']; ?></div>
        <?php
    endforeach;
?>
</div>
</div>
</div>

<div class="grid">
<?php    
    
    foreach($artists as $artist) :
        ?>
        <div class="nni_artist <?php echo $artist->nni_artist_instagram_hashtags; ?>"><?php echo $artist->ID; ?></div>
        <?php
    endforeach;
?>
</div>

<?php


    // END

    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}
add_shortcode('nni_annuaire', 'nni_shortcode_annuaire');

function nni_check_function() {
    global $post;

    if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'nni_annuaire') ) {
        wp_enqueue_style( 'nni-styles', plugin_dir_url( __FILE__ ) . 'css/nni_annuaire.css', [], time()); //'1.0.1' );
        

        wp_register_script( 'nni-isotope', plugin_dir_url( __FILE__ ) . 'js/isotope.pkgd.min.js', array( 'jquery' ), '1.1', true );
        wp_enqueue_script( 'nni-isotope' );
        wp_register_script( 'nni-annuaire', plugin_dir_url( __FILE__ ) . 'js/nni-annuaire.js', array( 'nni-isotope' ), time(), true );
        wp_enqueue_script( 'nni-annuaire' );
    }
}

add_action('wp_enqueue_scripts', 'nni_check_function');