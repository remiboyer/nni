<?php
/**
 * Plugin Name: Nicolas Nova Instagram
 * Description: Annuaire comédiens synchronisé avec compte Instagram
 * Author: Rémi Boyer
 * Version: 1.1.62
 */
/**
 * GitLab Plugin URI: https://gitlab.com/remiboyer/nni
 */

namespace NNI\Plugin;
use WP_Dependency_Installer;
use EspressoDev\InstagramBasicDisplay\InstagramBasicDisplay; 


require_once plugin_dir_path( __FILE__ ) . '/vendor/autoload.php';
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Charge notre dépendance Carbon Fields via Composer
 */
function load_carbonfields() {
	\Carbon_Fields\Carbon_Fields::boot();
}
add_action( 'after_setup_theme', __NAMESPACE__ . '\\load_carbonfields' );

/**
 * Charge notre fichier de plugin
 *
 * @return mixed
 */
function load_plugin() {
    WP_Dependency_Installer::instance( __DIR__ )->run();
	require_once plugin_dir_path( __FILE__ ) . '/includes/options.php';

    // Custom post type
	require_once plugin_dir_path( __FILE__ ) . '/includes/cpt.php';

    // Shortcode annuaire
	// require_once plugin_dir_path( __FILE__ ) . '/includes/shortcode.php';
}
add_action( 'plugins_loaded', __NAMESPACE__ . '\\load_plugin' );
